package com.example.whatsapptwin.Adpters;

import static androidx.core.app.ActivityCompat.startActivityForResult;

import android.content.Context;
import android.content.Intent;
import android.provider.MediaStore;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Lifecycle;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import com.example.whatsapptwin.HomePageFragments.BlankFragment;
import com.example.whatsapptwin.HomePageFragments.CamFragment;
import com.example.whatsapptwin.HomePageFragments.ChatFragment;
import com.example.whatsapptwin.HomePageFragments.StatusFargment;
import com.example.whatsapptwin.R;

public class VPAdapter extends FragmentStateAdapter {
Context context;
    public VPAdapter(@NonNull FragmentManager fragmentManager, @NonNull Lifecycle lifecycle) {
        super(fragmentManager, lifecycle);
    }

    @NonNull
    @Override
    public Fragment createFragment(int position) {
        switch (position){




            case 1:
                return new ChatFragment();


            case 2:
                return new StatusFargment();

            default:
                return new BlankFragment();

        }

    }


    @Override
    public int getItemCount() {
        return 3;
    }

}

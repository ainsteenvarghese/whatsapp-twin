package com.example.whatsapptwin.Adpters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.whatsapptwin.Datas.ChatData;
import com.example.whatsapptwin.R;

import java.util.ArrayList;

public class MyChatdataAdapter extends RecyclerView.Adapter<MyChatdataAdapter.MyViewHolder> {
    Context context;
    ArrayList<ChatData>chatData;
    LayoutInflater ly;

    public MyChatdataAdapter(Context context, ArrayList<ChatData> chatData) {
        this.context = context;
        this.chatData = chatData;
        this.ly = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = ly.inflate(R.layout.chat_blueprint , parent ,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        holder.chat_dp.setImageResource(chatData.get(position).getDp_chat());
        holder.chat_name.setText(chatData.get(position).getName_chat());
        holder.chat_last_msg.setText(chatData.get(position).getLast_msg_chat());
        holder.chat_last_date.setText(chatData.get(position).getLast_date_chat());
    }

    @Override
    public int getItemCount() {
        return chatData.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView chat_dp;
        TextView chat_name , chat_last_msg , chat_last_date;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            chat_dp = itemView.findViewById(R.id.dp);
            chat_name = itemView.findViewById(R.id.contact_name);
            chat_last_msg = itemView.findViewById(R.id.last_message);
            chat_last_date = itemView.findViewById(R.id.last_date);
        }
    }
}

package com.example.whatsapptwin.Datas;

public class ChatData {
    int dp_chat;
    String name_chat , last_msg_chat , last_date_chat;

    public ChatData(int dp_chat, String name_chat, String last_msg_chat, String last_date_chat) {
        this.dp_chat = dp_chat;
        this.name_chat = name_chat;
        this.last_msg_chat = last_msg_chat;
        this.last_date_chat = last_date_chat;
    }

    public int getDp_chat() {
        return dp_chat;
    }

    public void setDp_chat(int dp_chat) {
        this.dp_chat = dp_chat;
    }

    public String getName_chat() {
        return name_chat;
    }

    public void setName_chat(String name_chat) {
        this.name_chat = name_chat;
    }

    public String getLast_msg_chat() {
        return last_msg_chat;
    }

    public void setLast_msg_chat(String last_msg_chat) {
        this.last_msg_chat = last_msg_chat;
    }

    public String getLast_date_chat() {
        return last_date_chat;
    }

    public void setLast_date_chat(String last_date_chat) {
        this.last_date_chat = last_date_chat;
    }
}

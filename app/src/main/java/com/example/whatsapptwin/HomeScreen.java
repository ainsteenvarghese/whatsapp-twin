package com.example.whatsapptwin;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Lifecycle;
import androidx.viewpager.widget.ViewPager;
import androidx.viewpager2.widget.ViewPager2;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.widget.Toast;

import com.example.whatsapptwin.Adpters.VPAdapter;
import com.example.whatsapptwin.HomePageFragments.ChatFragment;
import com.example.whatsapptwin.HomePageFragments.StatusFargment;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.tabs.TabLayout;

//This activity fill have be holding other fragments..
//This Activity comes after Splash Screen.
public class HomeScreen extends AppCompatActivity {
Toolbar toolbar;
TabLayout tab;
ViewPager2 pager;
FloatingActionButton float_button;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_screen);
        toolbar = findViewById(R.id.toolbar);
        tab =findViewById(R.id.tab);
        pager = findViewById(R.id.page);
        float_button = findViewById(R.id.floatingActionButton);

        float_button.setImageResource(R.drawable.ic_baseline_chat_24);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                tab.getTabAt(1).select();

                pager.setCurrentItem(1);
            }
        },100);




        tab.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                pager.setCurrentItem(tab.getPosition());
                if (tab.getPosition() == 0) {
                    Intent cam = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(cam, 123);


                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            pager.setCurrentItem(1);
                        }
                    }, 600);

                }
                if (tab.getPosition() == 1) {
                    float_button.setImageResource(R.drawable.ic_baseline_chat_24);
                }
                if (tab.getPosition() == 2) {
                    float_button.setImageResource(R.drawable.camera_icon);

                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        pager.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
                tab.selectTab(tab.getTabAt(position));
            }
        });

        FragmentManager fm = getSupportFragmentManager();
        VPAdapter vpAdapter = new VPAdapter(fm , getLifecycle());
        pager.setAdapter(vpAdapter);
    }

    public void loadf(Fragment f){
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.frameLayout , f);
        ft.commit();
    }


}
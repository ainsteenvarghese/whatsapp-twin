
package com.example.whatsapptwin.HomePageFragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.whatsapptwin.Adpters.MyChatdataAdapter;
import com.example.whatsapptwin.Datas.ChatData;
import com.example.whatsapptwin.R;

import java.util.ArrayList;
//Chat Fragment with recycler view only...
//comes after HomeActivity..

public class ChatFragment extends Fragment {
RecyclerView rv_chat;
ArrayList<ChatData>chatData;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_chat, container, false);
        chatData = new ArrayList<>();
        rv_chat = view.findViewById(R.id.chat_rv);

        chatData.add(new ChatData(R.drawable.aa , "Akshay" , "cinema uno" , "5/21/22"));
        chatData.add(new ChatData(R.drawable.aa , "Akshay" , "cinema uno" , "5/21/22"));

        chatData.add(new ChatData(R.drawable.aa , "Akshay" , "cinema uno" , "5/21/22"));

        chatData.add(new ChatData(R.drawable.aa , "Akshay" , "cinema uno" , "5/21/22"));
        chatData.add(new ChatData(R.drawable.aa , "Akshay" , "cinema uno" , "5/21/22"));
        chatData.add(new ChatData(R.drawable.aa , "Akshay" , "cinema uno" , "5/21/22"));
        chatData.add(new ChatData(R.drawable.aa , "Akshay" , "cinema uno" , "5/21/22"));
        chatData.add(new ChatData(R.drawable.aa , "Akshay" , "cinema uno" , "5/21/22"));
        chatData.add(new ChatData(R.drawable.aa , "Akshay" , "cinema uno" , "5/21/22"));
        chatData.add(new ChatData(R.drawable.aa , "Akshay" , "cinema uno" , "5/21/22"));

        LinearLayoutManager l = new LinearLayoutManager(getContext());
        MyChatdataAdapter adapter = new MyChatdataAdapter(getContext() , chatData);
        rv_chat.setLayoutManager(l);
        rv_chat.setAdapter(adapter);

        return view;
    }
}